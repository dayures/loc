
Linked Open Colors
==================

This software (and dataset) has been created for the April Fools' Day of 2011,
as an informal initiative for promoting linked data.


AUTHORS
-------

 * Carlos Tejo <http://dayures.net/foaf.rdf#me>
 * Iván Mínguez <http://www.iminguez.com/foaf.rdf#me>
 * Diego Berrueta <http://berrueta.net/foaf.rdf#me>
 * Sergio Fernández <http://www.wikier.org/foaf#wikier>


THIRD PARTY LIBRARIES
---------------------

This project uses some third party libraries:

 * Mimeparse <https://code.google.com/archive/p/mimeparse/>


INSTALLING
----------

If you want to install this application, there are some prerequisites:

 * Install Django, 1.10 or greater 

And then you can launch it:

    python manage.py runserver

RUNNING USING GOOGLE APP ENGINE
-------------------------------
Follow the steps present in https://cloud.google.com/python/django/appengine

On Unix:
virtualenv env
env\scripts\activate
pip install -r requirements-vendor.txt -t lib/
pip install -r requirements.txt

* To run locally
python manage.py runserver 

* To deploy in GAE (the Google Cloud SDK MUST be installed)
python manage.py collectstatic
gcloud app deploy

* To test (generate the datasets files)
python manage.py test colors.test
