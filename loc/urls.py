"""loc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.http import HttpResponsePermanentRedirect
from colors.views import color
from colors.views import rgb_html, rgb_csv_html, css_html
from colors.views import rgb_rdf, rgb_csv_rdf, css_rdf
from colors.views import rgb_rdf_ttl, rgb_csv_rdf_ttl, css_rdf_ttl
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.conf import settings

regex_0_255 = '([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])'

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html')),
    url(r'^doc$', TemplateView.as_view(template_name='doc.html')),
    url(r'^color$', lambda request: HttpResponsePermanentRedirect("/")),
    url(r'^color/$', lambda request: HttpResponsePermanentRedirect("/")),
    url(r'^favicon.ico', lambda request: HttpResponsePermanentRedirect("/resources/images/favicon.ico")),
    url(r'^void', lambda request: HttpResponsePermanentRedirect("/resources/void.rdf")),

    url(r'^color/rgb/(?P<color>[0-9a-fA-F]{3})$', color, name='color'),
    url(r'^color/rgb/(?P<rgb>[0-9a-fA-F]{3}).html$', rgb_html, name='rgb_html'),
    url(r'^color/rgb/(?P<rgb>[0-9a-fA-F]{3}).rdf$', rgb_rdf, name='rgb_rdf'),
    url(r'^color/rgb/(?P<rgb>[0-9a-fA-F]{3}).ttl$', rgb_rdf_ttl, name='rgb_rdf_ttl'),

    url(r'^color/rgb/(?P<color>[0-9a-fA-F]{6})$', color, name='color'),
    url(r'^color/rgb/(?P<rgb>[0-9a-fA-F]{6}).html$', rgb_html, name='rgb_html'),
    url(r'^color/rgb/(?P<rgb>[0-9a-fA-F]{6}).rdf$', rgb_rdf, name='rgb_rdf'),
    url(r'^color/rgb/(?P<rgb>[0-9a-fA-F]{6}).ttl$', rgb_rdf_ttl, name='rgb_rdf_ttl'),

    url(r'^color/rgb/(?P<color>'+regex_0_255+','+regex_0_255+','+regex_0_255+')$', color, name='color'),
    url(r'^color/rgb/(?P<rgb>'+regex_0_255+','+regex_0_255+','+regex_0_255+').html$', rgb_csv_html, name='rgb_csv_html'),
    url(r'^color/rgb/(?P<rgb>'+regex_0_255+','+regex_0_255+','+regex_0_255+').rdf$', rgb_csv_rdf, name='rgb_csv_rdf'),
    url(r'^color/rgb/(?P<rgb>'+regex_0_255+','+regex_0_255+','+regex_0_255+').ttl$', rgb_csv_rdf_ttl, name='rgb_csv_rdf_ttl'),

    url(r'^color/css/(?P<color>\w+)$', color, name='color'),
    url(r'^color/css/(?P<css>\w+).html$', css_html, name='css_html'),
    url(r'^color/css/(?P<css>\w+).rdf$', css_rdf, name='css_rdf'),
    url(r'^color/css/(?P<css>\w+).ttl$', css_rdf_ttl, name='css_rdf_ttl')
] 

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
