#!/usr/bin/python
# -*- coding: utf-8 -

from SPARQLWrapper import SPARQLWrapper, JSON
import json
def retrieve_mappings():
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery("""
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dbo: <http://dbpedia.org/ontology/>
SELECT ?color ?hex ?label
FROM <http://dbpedia.org>
WHERE {
  ?color dbo:colourHexCode ?hex .
  { 
    SELECT ?color (MIN(?name) AS ?label) 
    WHERE {
      ?color foaf:name ?name .
      {
        SELECT ?color 
        FROM <http://dbpedia.org>
        WHERE {
          ?color rdf:type dbo:Colour ;
          dbo:colourHexCode ?hex .
        } GROUP BY ?color
        HAVING (count(distinct ?hex) = 1)
      }
    } GROUP BY ?color 
  }
}    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    mappings = {}

    for result in results["results"]["bindings"]:
        mappings[result["hex"]["value"]] = {'uri': result["color"]["value"], 'label': result["label"]["value"]} 

    return mappings

if __name__ == "__main__":
    mappings = retrieve_mappings()
    with open('mappings_dbpedia_colors.json', 'w') as outfile:
        json.dump(mappings, outfile, indent=4)
