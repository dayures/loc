from bs4 import BeautifulSoup
import requests
import json
from collections import OrderedDict

# Obtain Crayola Colors JSON scrapping the primary source crayola.com
def generate_crayola_from_crayola():
    url = "http://www.crayola.com/explore-colors/"
    r  = requests.get(url)
    data = r.text
     
    soup = BeautifulSoup(data, 'html.parser')
    colors = []
    for entry in soup.find_all('li', {"class": 'color-box'}):
        name = entry.find('a', {'id': 'colorLink'}).get_text().title()
        hexCode = entry.find('div',{"class": 'color-text-arrow'}).get("style")[-7:].upper()
        rgb = tuple(int(hexCode.lstrip('#')[i:i+2], 16) for i in (0, 2 ,4)) #kudos https://stackoverflow.com/a/29643643 
         
        color = OrderedDict()
        color["hex"] = hexCode
        color["name"] = name
        color["rgb"] = str(rgb)
         
        if " W/ " in color["name"]:
            print color["name"]
        else:
            colors.append(color)
         
    with open('mappings_crayola_colors-from-crayola.json', 'w') as outfile:
        json.dump(colors, outfile, indent=4)


# Obtain Crayola Colors JSON scrapping the wikepedia
def generate_crayola_from_wikipedia():
    url = "https://en.wikipedia.org/wiki/List_of_Crayola_crayon_colors"
    r  = requests.get(url)
    data = r.text
    
    soup = BeautifulSoup(data, 'html.parser')
    colors = []
    #//*[@id="mw-content-text"]/div/table[1]/tbody
    for entry in soup.find('div', {"id":"mw-content-text"}).find('div').find('table').find_all('tr'):
        if entry.find('th'):
            continue
        name = entry.find_all('td')[0].get_text().strip()
        #hexCode = entry.find('div',{"class": 'color-text-arrow'}).get("style")[-7:].upper()
        hexCode = entry.find_all('td')[7].get_text()[:7]
        rgb_generated = tuple(int(hexCode.lstrip('#')[i:i+2], 16) for i in (0, 2 ,4)) #kudos https://stackoverflow.com/a/29643643
        rgb_scrapping = (int(entry.find_all('td')[4].get_text()),int(entry.find_all('td')[5].get_text()),int(entry.find_all('td')[6].get_text()))  
    #     if rgb_generated != rgb_scrapping:
    #         print name
    #         print rgb_generated
    #         print rgb_scrapping
        color = OrderedDict()
        color["hex"] = hexCode
        color["name"] = name
        color["rgb"] = str(rgb_generated)
        
        colors.append(color)
        
    with open('mappings_crayola_colors-from-wikipedia.json', 'w') as outfile:
        json.dump(colors, outfile, indent=4)