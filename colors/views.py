
import logging
from colors.model import Color
from colors.http import Http303, get_preferred_suffix
from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response
from colors.common import if_else

def color(request, color):
    logging.info("Serving color '%s' with content negotiation" % color)
    suffix = get_preferred_suffix(request)
    url = "%s.%s" % (color, suffix)
    return Http303(url)

def color_html(request, color, format):
    logging.info("Serving %s color '%s' as HTML" % (format, color))
    try:
        ctx = { "color" : Color(color, format) }
        return render_to_response("color.html", ctx, content_type="application/xhtml+xml")
    except Exception as e:
       raise Http404(e)

def color_rdf(request, color, format):
    logging.info("Serving %s color #%s as RDF" % (format, color))
    try:
        #return HttpResponse(color.get_rdf_xml(), content_type="application/rdf+xml")
        ctx = { "color" : Color(color, format) }
        return render_to_response("color.rdf", ctx, content_type="application/rdf+xml")
    except Exception as e:
       raise Http404(e)

def color_rdf_ttl(request, color, format):
    logging.info("Serving %s color #%s as RDF" % (format, color))
    try:
        ctx = { "color" : Color(color, format) }
        return render_to_response("color.ttl", ctx, content_type="text/turtle")
    except Exception as e:
       raise Http404(e)

################################################################################

def rgb_html(request, rgb):
    format = if_else(len(rgb)==3, "rgb", "rrggbb")
    return color_html(request, rgb, format)

def rgb_rdf(request, rgb):
    format = if_else(len(rgb)==3, "rgb", "rrggbb")
    return color_rdf(request, rgb, format)

def rgb_rdf_ttl(request, rgb):
    format = if_else(len(rgb)==3, "rgb", "rrggbb")
    return color_rdf_ttl(request, rgb, format)

################################################################################

def rgb_csv_html(request, rgb):
    return color_html(request, rgb, "r,g,b")

def rgb_csv_rdf(request, rgb):
    return color_rdf(request, rgb, "r,g,b")

def rgb_csv_rdf_ttl(request, rgb):
    return color_rdf_ttl(request, rgb, "r,g,b")

################################################################################

def css_html(request, css):
    return color_html(request, css, "css")

def css_rdf(request, css):
    return color_rdf(request, css, "css")

def css_rdf_ttl(request, css):
    return color_rdf_ttl(request, css, "css")
