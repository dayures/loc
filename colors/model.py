
from colors.common import if_else, format_two_decimal
import colorsys
#from rdflib import ConjunctiveGraph, Namespace, URIRef, Literal
from colors.mappings import ColorsMap

formats = {
            "rgb"    : "rgb",
            "rrggbb" : "rgb",
            "r,g,b"  : "rgb",
            "css"    : "css"
         }

class Color:

    def __init__(self, color, format, base="http://purl.org/colors"):
        self.base = base
        self.color = color
        self.format = format

        self.rgb = self.__parse_color() # tuple of decimal values (0..255, 0..255, 0..255)
        self.hex = self.__format_hex() # One value. All uppercase
        self.css = self.__format_css() # Dictionary with keys name, uri

        self.rgb_percent = self.__format_rgb_percent()
        self.rgb_percent_print = tuple([format_two_decimal(x) for x in self.rgb_percent])
        self.rgb_decimal_percent = self.__format_rgb_decimal_percent()
        self.rgb_decimal_percent_print = tuple([format_two_decimal(x) for x in self.rgb_decimal_percent])

        self.hsv = self.__format_hsv() # tuple (0-100, 0-100, 0-100)
        self.hls = self.__format_hls() # tuple (0-100, 0-100, 0-100)
        self.hsl = self.__format_hsl() # tuple (0-100, 0-100, 0-100)
		
        self.hue = format_two_decimal(self.__hue()) # Calculated. In degrees [0-360] (H=hue. valid for HSL and HSV, because they have the same hue).
        self.hsl_saturation = format_two_decimal(self.__hsl_saturation()) # Direct. Percentage [0-100] (S in HSL)
        self.hsv_saturation = format_two_decimal(self.__hsv_saturation()) # Direct. Percentage [0-100] (S in HSV)
        self.hsl_lightness  = format_two_decimal(self.__hsl_lightness()) # Direct. Percentage [0-100]  (L in HSL)
        self.hsv_value = format_two_decimal(self.__hsv_value()) # Direct. Percentage [0-100] (V in HSV)
        
        self.cmyk = self.__format_cmyk() # dictionary with keys 'c', 'm', 'y', 'k'

        self.__build_uris()
        self.__build_mappings()

    def __parse_color(self):
        if (self.format == "rgb"):
            return (int(self.color[0], 16) * 16 + int(self.color[0], 16), 
                    int(self.color[1], 16) * 16 + int(self.color[1], 16), 
                    int(self.color[2], 16) * 16 + int(self.color[2], 16))
        elif (self.format == "rrggbb"):
            return (int(self.color[0:2], 16), int(self.color[2:4], 16), int(self.color[4:6], 16))
        elif (self.format == "r,g,b"):
            splitted = self.color.split(",")
            r = int(splitted[0])
            g = int(splitted[1])
            b = int(splitted[2])
            if (r > 255 or g > 255 or b > 255):
                raise Exception("Invalid color: " + self.color)
            else:
                return (r, g, b)
        elif (self.format == "css"):
            rgb = ColorsMap.get_mapping_css_color(self.color)
            if (rgb == None):
                raise Exception("Invalid CSS color: " + self.color)
            else:
                self.color = self.color.lower()
                return (int(rgb[0:2], 16), int(rgb[2:4], 16), int(rgb[4:6], 16))
        else:
            raise Exception("Unssuported format: " + self.format)

    def __format_css(self):
        if (self.format in ["rgb", "rrggbb", "r,g,b"]):
            css_name = ColorsMap.get_reverse_mapping_css_color(self.hex) # both hex (sent one and the ones in CSS file) in uppercase
            if css_name:
                css_uri = "%s/%s/%s" % (self.base, formats["css"], css_name.lower())
                return {'name': css_name.lower(), 'uri': css_uri}
        elif (self.format == "css"):
            css_uri = "%s/%s/%s" % (self.base, formats["css"], self.color.lower())
            return {'name': self.color.lower(), 'uri': css_uri}
        else:
            raise Exception("Unssuported format: " + self.format)

    def __str__(self):
        if (self.format == "rgb" or self.format == "rrggbb"):
            return "#%s" % self.color
        elif (self.format == "r,g,b"):
            return "rgb(%s)" % self.color
        else:
            return self.color

    def __format_hex(self):
        if (self.format == "rrggbb"):
            return self.color.upper()
        else:
            r = if_else(self.rgb[0]<16, "0%X" % self.rgb[0], "%X" % self.rgb[0])
            g = if_else(self.rgb[1]<16, "0%X" % self.rgb[1], "%X" % self.rgb[1])
            b = if_else(self.rgb[2]<16, "0%X" % self.rgb[2], "%X" % self.rgb[2])
            return "%s%s%s" % (r, g, b)
    
    def __build_uris(self):
        self.uri = "%s/%s/%s" % (self.base, formats[self.format], self.color)
        if (self.color == (self.hex).lower()):
            self.primary_uri = self.uri
        else: # rgb hex with not all in lowercase, CSS
            self.primary_uri = "%s/%s/%s" % (self.base, formats["rrggbb"], (self.hex).lower())
        self.rgb_decimal_uri = "%s/%s/%s" % (self.base, formats["r,g,b"], ("%s,%s,%s" % (self.rgb[0], self.rgb[1], self.rgb[2])))

    def __build_mappings(self):
        self.mappings = {}
        self.mappings["dbpedia_color"] = ColorsMap.get_mapping_dbpedia_color(self.hex)
        self.mappings["crayola_color"] = ColorsMap.get_mapping_crayola_color(self.hex)
        self.mappings["ral_color"] = ColorsMap.get_mapping_ral_color(self.hex)

    # valid values: 0..100
    def __format_rgb_percent(self):
        # kudos https://github.com/ubernostrum/webcolors
        # In order to maintain precision for common values,
        # 256 / 2**n is special-cased for values of n
        # from 0 through 4, as well as 0 itself.
        specials = {255: 100, 128: 50, 64: 25,
                    32: 12.5, 16: 6.25, 0: 0}
        return tuple([specials.get(d, (d / 255.0)*100) for d in self.rgb])

    # valid values: [0.0 .. 1.0]    
    def __format_rgb_decimal_percent(self):
        return tuple([float(p)/100 for p in self.rgb_percent])

    def __format_hsv(self):
        # colorsys.rgb_to_hsv returns values from 0.0 to 1.0 for all tuple's values
        return tuple([d*100 for d in colorsys.rgb_to_hsv(self.rgb_decimal_percent[0], self.rgb_decimal_percent[1], self.rgb_decimal_percent[2])])

    def __format_hls(self):
        # colorsys.rgb_to_hls returns values from 0.0 to 1.0 for all tuple's values
        return tuple([d*100 for d in colorsys.rgb_to_hls(self.rgb_decimal_percent[0], self.rgb_decimal_percent[1], self.rgb_decimal_percent[2])])

    def __format_hsl(self):
        return tuple([self.hls[0], self.hls[2], self.hls[1]])
    
    def __hue(self):
        # kudos https://github.com/ubernostrum/webcolors
        # In order to maintain precision for common values
        specials = {360: 100, 180: 50, 90: 25,
                    45: 12.5, 12.5: 6.25, 0: 0}
        return specials.get(self.hls[0], (self.hls[0]/100)*360)
        
    def __hsl_saturation(self):
        return self.hsl[1]

    def __hsv_saturation(self):
        return self.hsv[1]
        
    def __hsv_value(self):
        return self.hsv[2]
    
    def __hsl_lightness(self):
        return self.hsl[2]
        
    #def get_rdf(self):
    #    if (self.graph == None):
    #        self.graph = self.__get_rdf()
    #    return self.graph

    #def __get_rdf(self):
    #    graph = ConjunctiveGraph()
    #    RDF = Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#")
    #    RDFS  = Namespace("http://www.w3.org/2000/01/rdf-schema#")
    #    FOAF  = Namespace("http://xmlns.com/foaf/0.1/")
    #    LOCO  = Namespace("%s/loco#" % self.base)
    #    graph.bind("rdfs", RDFS)
    #    graph.bind("rdf", RDF)
    #    graph.bind("foaf", FOAF)
    #    graph.bind("loco", LOCO)
    #
    #    doc = BNode()
    #    graph.add((doc, RDF.type, FOAF["Document"]))
    #    color = URIRef(self.uri)
    #    graph.add((color, RDF.type, LOCO["Color"]))
    #    graph.add((color, RDFS.label, Literal(str(color))))
    #    graph.add((color, FOAF.page, URIRef("%s/%s.html" % (self.base, self.rgb))))
    #
    #    return graph

    #def get_rdf_xml(self):
    #    graph = self.get_rdf()
    #    return graph.serialize(format="pretty-xml")

    def __format_cmyk(self):
        cmyk = [format_two_decimal(x) for x in rgb_to_cmyk(self.rgb[0], self.rgb[1], self.rgb[2])]
        return {'c': cmyk[0], 'm': cmyk[1], 'y': cmyk[2], 'k': cmyk[3]}

def rgb_to_cmyk(r,g,b):
    #kudos https://stackoverflow.com/a/14088415
	cmyk_scale = 100
	if (r == 0) and (g == 0) and (b == 0):
		# black
		return 0, 0, 0, cmyk_scale

	# rgb [0,255] -> cmy [0,1]
	c = 1 - r / 255.
	m = 1 - g / 255.
	y = 1 - b / 255.

	# extract out k [0,1]
	min_cmy = min(c, m, y)
	c = (c - min_cmy) / (1 - min_cmy)
	m = (m - min_cmy) / (1 - min_cmy)
	y = (y - min_cmy) / (1 - min_cmy)
	k = min_cmy

	# rescale to the range [0,cmyk_scale]
	return c*cmyk_scale, m*cmyk_scale, y*cmyk_scale, k*cmyk_scale
