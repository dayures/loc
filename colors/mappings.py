
import os
from colors.common import KeyInsensitiveDict

class ColorsMap:
    path_css = os.path.join(os.path.dirname(__file__), "..", "data", "mappings_css_colors.json")
    path_dbpedia = os.path.join(os.path.dirname(__file__), "..", "data", "mappings_dbpedia_colors.json")
    path_crayola = os.path.join(os.path.dirname(__file__), "..", "data", "mappings_crayola_colors-from-wikipedia.json")
    path_ral = os.path.join(os.path.dirname(__file__), "..", "data", "mappings_ral_colors.json")
    mappings_css = eval(open(path_css).read())
    mappings_dbpedia = eval(open(path_dbpedia).read())
    mappings_crayola = {c['hex'].replace("#",""):c  for c in eval(open(path_crayola).read())} # adapt the json structure
    mappings_ral = {c['hex']:c  for c in eval(open(path_ral).read())} # adapt the json structure

    def __init__(self):
        pass

    @classmethod
    def get_mapping_color(cls, color, mappings, case_insensitive=False, reverse=False):
        if reverse:
            mappings = {v: k for k, v in mappings.iteritems()}
        if (case_insensitive):
            mappings = KeyInsensitiveDict(mappings)
        if (color in mappings):
            return mappings[color]
        else:
            return None

    @classmethod
    def get_mapping_dbpedia_color(cls, color_hex):
        return cls.get_mapping_color(color_hex, cls.mappings_dbpedia)

    @classmethod
    def get_mapping_css_color(cls, color_name):
        return cls.get_mapping_color(color_name, cls.mappings_css, case_insensitive=True)
        
    @classmethod
    def get_reverse_mapping_css_color(cls, color_hex):
        return cls.get_mapping_color(color_hex, cls.mappings_css, case_insensitive=True, reverse=True)

    @classmethod
    def get_mapping_crayola_color(cls, color_hex):
        return cls.get_mapping_color(color_hex, cls.mappings_crayola)

    @classmethod
    def get_mapping_ral_color(cls, color_hex):
        return cls.get_mapping_color(color_hex, cls.mappings_ral)
