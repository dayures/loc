RAL
===
#. The JSON file was generated using scrapping techniques. The script generator file is missed.

dbpedia
=======
#. Run the python program mappings/mappings_dbpedia_colors.py 
#. It stores the result in a file mappings_dbpedia_colors.json
#. Copy this file to the data folder

* SPARQL endpoint to query: http://dbpedia.org/sparql
* In dbpedia, select the colors with just ONE dbo:colourHexCode.
* In case there are more than one foaf:name for this resource, select the minimun one (we took this decision).
* The export by CSV, TSV is not working (missing minName, it appears 0)
* Using SPARQL subqueries capabilities (https://www.w3.org/TR/sparql11-query/#subqueries http://vos.openlinksw.com/owiki/wiki/VOS/VirtTipsAndTricksSPARQL11Subquery)


CSS
===
In the CSS mapping is included HTML 4 + CSS 1 + CSS 2 + CSS 2.1 + CSS 3 (each one is subset of the previous one).
The matching is case insensitive.


HTML 4
------
HTML 4 defined `two ways to specify sRGB colors`_. One is :

* A set of predefined color names which correspond to specific
  hexadecimal values; for example, ``blue``. HTML 4 defines **16**
  such colors.


CSS 1
-----
In `its description of color units`_, CSS 1 suggested 
a set of sixteen color names. These names were
**identical to the set defined in HTML 4**, but CSS 1 did not provide
definitions of their values and stated that they were taken from "the
Windows VGA palette".


CSS 2
-----
In its `section on colors`_, CSS 2 allowed the same methods of
specifying colors as CSS 1, and defined and provided values for
**16 named colors**, identical to the set found in HTML 4.

The CSS 2.1 revision did not add any new methods of specifying sRGB
colors, but did define **1** `one additional named color`_: ``orange``.


CSS 3
-----
CSS 3 defines a new set of **147 color names**. This set is taken
directly from `the named colors defined for SVG (Scalable Vector
Graphics)`_ markup, and is a superset of the named colors defined in
CSS 2.1.


.. _two ways to specify sRGB colors: http://www.w3.org/TR/html401/types.html#h-6.5
.. _its description of color units: http://www.w3.org/TR/CSS1/#color-units
.. _section on colors: http://www.w3.org/TR/CSS2/syndata.html#color-units
.. _a list of names of system colors: http://www.w3.org/TR/CSS2/ui.html#system-colors
.. _one additional named color: https://www.w3.org/TR/CSS21/changes.html#q21.2
.. _the named colors defined for SVG (Scalable Vector Graphics): http://www.w3.org/TR/SVG11/types.html#ColorKeywords
